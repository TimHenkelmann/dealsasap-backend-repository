var BuyingOffer = require('./buyingOfferSchema');



exports.postBuyingOffer = function(req, res) {

    var buyingOffer = new BuyingOffer(req.body);

    //do not allow user to fake identity. The user who posted the buying offer must be the same user that is logged in
    //STILL NEEDS IMPLEMENTATION! Currently fails with error
    /*if (!req.user.equals(buyingOffer.user)) {
        res.sendStatus(401);
    }*/

    buyingOffer.save(function(err, m) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        res.status(201).json(m);
    });
};


// Create endpoint /api/buyingoffers for GET
exports.getBuyingOffers = function(req, res) {
    BuyingOffer.find(function(err, buyingOffers) {
        if (err) {
            res.status(500).send(err);
            return;
        }
        res.json(buyingOffers);
    });
};


// Create endpoint /api/buyingoffers/:buyingoffer_id for GET
exports.getBuyingOffer = function(req, res) {
    // Use the Beer model to find a specific beer
    BuyingOffer.findById(req.params.buyingOffer_id, function(err, buyingoffer) {
        if (err) {
            res.status(500).send(err)
            return;
        };

        res.json(buyingoffer);
    });
};

// Create endpoint /api/buyingoffers/:buyingoffer_id for PUT
exports.putBuyingoffer = function(req, res) {
    // Use the Beer model to find a specific beer
    BuyingOffer.findByIdAndUpdate(
        req.params.buyingOffer_id,
        req.body,
        {
            //pass the new object to cb function
            new: true,
            //run validations
            runValidators: true
        }, function (err, buyingoffer) {
            if (err) {
                res.status(500).send(err);
                return;
            }
            res.json(buyingoffer);
        });

};

// Create endpoint /api/buyingOffers/:buyingoffer_id for DELETE
exports.deleteBuyingOffer = function(req, res) {
    // Use the Beer model to find a specific beer and remove it
    BuyingOffer.findById(req.params.buyingOffer_id, function(err, buyingoffer) {
        if (err) {
            res.status(500).send(err)
            return;
        };

        //authorize
        //if (buyingoffer.user && req.user.equals(buyingoffer.user)) {
        if (true) {
            buyingoffer.remove();
            res.sendStatus(200);
        } else {
            res.sendStatus(401);
        }

    });
};
