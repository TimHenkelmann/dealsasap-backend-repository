module.exports = buyingofferRoutes;

function buyingofferRoutes(passport) {

    var buyingofferController = require('./buyingofferController');
    var router = require('express').Router();
    var unless = require('express-unless');

    var mw = passport.authenticate('jwt', {session: false});
    mw.unless = unless;

    //middleware
    router.use(mw.unless({method: ['GET', 'OPTIONS']}));

    router.route('/buyingoffers')
        .post(buyingofferController.postBuyingoffer)
        .get(buyingofferController.getBuyingoffers);

    router.route('/buyingoffers/:buyingoffer_id')
        .get(buyingofferController.getBuyingoffer)
        .put(buyingofferController.putBuyingoffer)
        .delete(buyingofferController.deleteBuyingoffer);
    return router;
}



